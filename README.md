This houses scripts to install openSUSE from within another GNU/Linux distribution. Useful for VPS hosting providers not offering an option to upload custom ISO images.

Example for OVH's Debian based "rescue system":
```
./boostrap-ovh.sh /dev/sdb bios toor unlock
```
Where `/dev/sdb` is the disk to install the system on (it will be wiped), `bios` is the boot mode (alternative would be `efi`), `toor` is the root passphrase, and `unlock` is the LUKS passphrase.

There is lots to be improved in the bootstrap script to make it more universal - currently it's assuming a configuration I personally like.

Known bugs:
 - after the first boot, the crypto disk might not be found - reboot, ./rescue.sh, `dracut '' $kernel -f`, reboot
