#!/bin/sh
set -Ceu

disk="$1"
mode="$2"
rootpass="$3"
lukspass="$4"

if [ -z "$disk" ]
then
	echo 'Please specify a disk.'
	exit 1
fi

if [ ! -b "$disk" ]
then
	echo 'Specified disk cannot possibly be a disk.'
	exit 1
fi

if [ -z "$mode" ]
then
	mode='bios'
fi

if [ "$mode" = 'efi']
then
	if [ ! -d "$fstab-mode" ]
	then
		echo 'Please provide your own fstab script.'
		exit 1
	fi
fi

if ! command -v mkfs.btrfs
then
	echo 'BtrFS progs not installed.'
	exit 1
fi
if ! command -v zypper
then
	echo 'Zypper not installed.'
	exit 1
fi
if ! command -v cryptsetup
then
	echo 'Cryptsetup not installed.'
	exit 1
fi

bios='1'
boot='2'
swap='3'
root='4'

echo 'Wiping your data ...'
fdisk -w always "$disk" < "fdisk-$mode"

echo 'Formatting encrypted partitions ...'
./makeluks.sh "$disk" "$lukspass"

echo 'Creating filesystems ...'
mkfs.btrfs -f "${disk}${boot}"
mkfs.btrfs /dev/mapper/cr_root

echo 'Mount root ...'
mount /dev/mapper/cr_root /mnt

echo 'Creating subvolumes ...'
./makesubvols.sh
btrfs subvolume set-default $(btrfs subvolume list /mnt | grep "@/.snapshots/1/snapshot" | grep -oP '(?<=ID )[0-9]+') /mnt

echo 'Remounting root ...'
umount /mnt
mount /dev/mapper/cr_root /mnt

echo 'Creating directory structure ...'
set -x
mkdir /mnt/.snapshots
mkdir /mnt/home
mkdir /mnt/opt
mkdir /mnt/root
mkdir /mnt/srv
mkdir /mnt/tmp
mkdir /mnt/usr
mkdir /mnt/usr/local
mkdir /mnt/var
set +x

echo 'Mounting subvolumes ...'
./mountsubvols.sh

echo 'Mounting boot ...'
mkdir /mnt/boot
mount "${disk}${boot}" /mnt/boot
mkdir /mnt/boot/grub2
mkdir /mnt/boot/grub2/i386-pc

echo 'Mounting slaves ...'
for dir in proc sys dev run
do
        mkdir "/mnt/$dir"
done
mount --types proc /proc /mnt/proc
mount --rbind /sys /mnt/sys
mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev
mount --make-rslave /mnt/dev
mount --bind /run /mnt/run
mount --make-slave /mnt/run

echo 'Enabling swap partition ...'
mkswap /dev/mapper/cr_swap
swapon /dev/mapper/cr_swap

echo 'Bootstrapping packages ...'
zypper --root /mnt ar --refresh https://download.opensuse.org/distribution/leap/15.4/repo/oss/ OSS
zypper --root /mnt ref
zypper -n --root /mnt in --no-recommends \
	kernel-default grub2 zypper vim-small shadow util-linux wicked iputils btrfsprogs openssh-server dmraid openSUSE-release openSUSE-build-key ca-certificates

echo 'Generating fstab ...'
rootuuid="$(blkid -o value -s UUID /dev/mapper/cr_root)"
swapuuid="$(blkid -o value -s UUID /dev/mapper/cr_swap)"
bootuuid="$(blkid -o value -s UUID ${disk}${boot})"
tee /mnt/etc/fstab <<EOF
UUID=$rootuuid  /                      btrfs  defaults              0  0
UUID=$bootuuid  /boot                  btrfs  defaults              0  0
#UUID=$swapuuid  swap                   swap   defaults              0  0
UUID=$rootuuid  /.snapshots            btrfs  subvol=/@/.snapshots  0  0
UUID=$rootuuid  /var                   btrfs  subvol=/@/var         0  0
UUID=$rootuuid  /usr/local             btrfs  subvol=/@/usr/local   0  0
UUID=$rootuuid  /tmp                   btrfs  subvol=/@/tmp         0  0
UUID=$rootuuid  /srv                   btrfs  subvol=/@/srv         0  0
UUID=$rootuuid  /root                  btrfs  subvol=/@/root        0  0
UUID=$rootuuid  /opt                   btrfs  subvol=/@/opt         0  0
UUID=$rootuuid  /home                  btrfs  subvol=/@/home        0  0
EOF

echo 'Generating crypttab ...'
rawswapuuid="$(blkid -o value -s UUID ${disk}${swap})"
tee /mnt/etc/crypttab <<EOF
cr_swap UUID=$rawswapuuid  /dev/urandom  swap
EOF

echo 'Writing network configuration ...'
tee /mnt/etc/sysconfig/network/ifcfg-eth0 <<EOF
BOOTPROTO=dhcp
STARTMODE=auto
EOF

echo 'Installing bootloader ...'
kernel="$(ls /mnt/lib/modules)"
sed -i -e 's/GRUB_CMDLINE_LINUX_DEFAULT=.*/GRUB_CMDLINE_LINUX_DEFAULT="splash=verbose loglevel=3"/' /mnt/etc/default/grub
chroot /mnt <<EOS
dracut '' $kernel
grub2-install $disk
grub2-mkconfig -o /boot/grub2/grub.cfg
echo 'Setting root passphrase ...'
echo 'root:$rootpass' | chpasswd
echo 'Enabling sshd ...'
systemctl enable sshd
EOS
if [ "$mode" = 'efi' ]; then
echo 'Installing shim ...'
chroot /mnt <<EOS
shim-install --config-file=/boot/grub2/grub.cfg
EOS
fi

echo 'Entering chroot for manual tasks ...'
chroot /mnt

echo 'Unmounting ...'
umount -R /mnt
cryptsetup luksClose cr_root
cryptsetup luksClose cr_swap
