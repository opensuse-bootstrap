#!/bin/sh
set -Ceux

partswap="${1}3"
partroot="${1}4"
lukspass="$2"

echo "$lukspass" | cryptsetup -q luksFormat "$partswap"
echo "$lukspass" | cryptsetup -q luksFormat "$partroot"

echo "$lukspass" | cryptsetup -q luksOpen "$partswap" cr_swap
echo "$lukspass" | cryptsetup -q luksOpen "$partroot" cr_root
