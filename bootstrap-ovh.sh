#!/bin/sh
set -Ceux

apt install -y btrfs-progs cryptsetup zypper
./bootstrap.sh "$1" "$2" "$3" "$4"
