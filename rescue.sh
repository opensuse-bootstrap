#!/bin/sh
set -Ceu

cryptsetup luksOpen "$1" cr_root
mount /dev/mapper/cr_root /mnt
mount /dev/mapper/cr_root /mnt/.snapshots -o subvol=@/.snapshots
mount /dev/mapper/cr_root /mnt/home -o subvol=@/home
mount /dev/mapper/cr_root /mnt/opt -o subvol=@/opt
mount /dev/mapper/cr_root /mnt/root -o subvol=@/root
mount /dev/mapper/cr_root /mnt/srv -o subvol=@/srv
mount /dev/mapper/cr_root /mnt/tmp -o subvol=@/tmp
mount /dev/mapper/cr_root /mnt/usr/local -o subvol=@/usr/local
mount /dev/mapper/cr_root /mnt/var -o subvol=@/var

mount "$2" /mnt/boot
mount --types proc /proc /mnt/proc
mount --rbind /sys /mnt/sys
mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev
mount --make-rslave /mnt/dev
mount --bind /run /mnt/run
mount --make-slave /mnt/run

chroot /mnt
